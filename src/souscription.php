<?php 
$title = 'Souscription';
include('include/articleHeader.php');

/* Return $_GET[$name] if it exists or else a default value. */
function getOrDefault($name, $default_value="") {
    if(!isset($_GET[$name])) {
        return $default_value;
    }
    return $_GET[$name];
}

/* Check if all required form data were sent through POST method. */
function isPostDataValid() {
    $valid = true;
    $required = ["surname", "forename", "phone", "language", "level",
                 "date", "password"];
    foreach($required as $field) {
        if( !array_key_exists($field, $_POST)) {
            $valid = false;
        }
    }
    /* Require at least one timeslot. */
    if (!isset($_POST["morning"]) && !isset($_POST["afternoon"])) {
        $valid = false;
    }
    /* Should also probably manually validate fields. */
    /* POST data can be sent even without using a form with arbitrary data. */
    return $valid;
}

/* Generate code from POST. isPostDataValid should probably be called */
/* before this function to check all required data is available. */
function generateCodeFromPost() {
    $res = "";
    $required = ["surname", "forename", "phone", "language", "level", "date"];
    foreach ($required as $field) {
        $res .= sanitize($_POST[$field]) . "_";
    }
    /* Special handling of checkboxes. */
    foreach (["morning", "afternoon"] as $checkbox) {
        if (isset($_POST[$checkbox])) {
            $res .= $checkbox . "_";
        }
    }
    $res = substr($res, 0, -1);
    return vigenere($res, sanitize($_POST["password"]));
}

function selectedHelper($targetLevel, $curLevel) {
    if ($targetLevel == $curLevel) {
        return "selected";
    }
    return "";
}

function checkedHelper($checkName) {
    if (isset($_GET[$checkName])) {
        return "checked";
    }
    return "";
}

?>

<?php if (($_SERVER["REQUEST_METHOD"] === "POST") && !isPostDataValid()): ?>
<section>
    <p class="error-text">
        Certains des champs requis ne sont pas renseignés
        (rappel: au moins un créneau doit être selectionné).
    </p>
</section>
<?php elseif (($_SERVER["REQUEST_METHOD"] === "POST") && isPostDataValid()): ?>
<section>
    <div class="info-text">
        <p>Code généré avec succès&nbsp;:&nbsp;</p>
        <p class="code"><?=generateCodeFromPost();?></p>
    </div>
</section>
<?php endif; ?>
<section>
    <h1>Souscrire à des cours en ligne</h1>
    <p> <span class="gen-eki">Gen-eki</span> (<span lang="ja">現役</span>)
        propose depuis peu une offre de formation en ligne pour ceux désirant
        un retour direct durant leur apprentissage ainsi que des conseils
        d'ordre technique ou méthodologique.</p>
    <p>Pour vous inscrire à un de nos cours, prière de remplir et de soumettre
        le formulaire plus bas, de conserver une copie du code unique généré et
        du mot de passe utilisé qui serviront de preuve de souscription avant
        le début de la session d'étude. </p>
    <p>Note: Le code est généré par le chiffre de Vigenère en utilisant le
        mot de passe en tant que clef. </p>
    <details> <summary>À propos des codes</summary> Les codes sont générés à
        partir du mot de passe qui ne doit contenir que des lettres de
        l'alphabet. Un préremplissage du formulaire est possible si l'on
        connait le code et son mot de passe associé. Essayez en renseignant le
        second formulaire avec le mot de passe <span class="code">bleu</span>
        et le code &nbsp;:&nbsp; <p class="code">
            Stqfjykys_Vih_+11772888773_zsprwi_1_2670-14-82_qisymhh </p>
    </details>
    <form method="post" action="souscription.php">
        <fieldset>
            <legend>Informations personnelles</legend>
            <div>
                <div>
                    <label for="surname">Nom&nbsp;:&nbsp;</label>
                    <input
                        type="text"
                        id="surname"
                        name="surname"
                        value="<?=getOrDefault('surname');?>"
                        required>
                </div>
                <div>
                    <label for="forename">Prénom&nbsp;:&nbsp</label>
                    <input
                        type="text"
                        id="forename"
                        name="forename"
                        value="<?=getOrDefault('forename');?>"
                        required>
                </div>
                <div>
                    <label for="phone">
                        Numéro de téléphone&nbsp;:&nbsp
                    </label>
                    <input
                        type="tel"
                        id="phone"
                        name="phone"
                        placeholder="+33123456789"
                        pattern="(\+33|0033)[0-9]{9}"
                        title="utilisez l'indicatif international +33"
                        value="<?=getOrDefault('phone');?>"
                        required>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Options du cours</legend>
            <div>
                <div>
                    <label>
                        Enseigné en langue&nbsp;:&nbsp;
                    </label>
                    <div>
                        <div>
                            <input
                                type="radio"
                                id="french"
                                name="language"
                                value="french"
                                <?php
                                if (!isset($_POST["language"]) || 
                                    ($_POST["language"] == "french")) {
                                    echo 'checked';
                                }
                                ?>
                            >
                            <label for="french">française</label>
                        </div>
                        <div>
                            <input
                                type="radio"
                                id="english"
                                name="language"
                                value="english"
                                <?php
                                if (getOrDefault("language") == "english") {
                                    echo 'checked';
                                }
                                ?>
                            >
                            <label for="english">anglaise</label>
                        </div>
                    </div>
                </div>
                <div>
                    <label for="level">
                        Niveau de japonais&nbsp;:&nbsp
                    </label>
                    <select
                        id="level"
                        name="level"
                        size="1"
                        <?php
                        $level = getOrDefault("level", -1);
                        if ($level < 1 || $level > 5) {
                            $level = 2;
                        }
                        ?>
                    >
                        <optgroup label="Débutant">
                            <option
                                value="1"
                                <?=selectedHelper($level, 1);?>
                            >
                                A1
                            </option>
                            <option
                                value="2"
                                <?=selectedHelper($level, 2);?>
                            >
                                A2
                            </option>
                        </optgroup>	
                        <optgroup label="Intermédiaire">
                            <option
                                value="3"
                                <?=selectedHelper($level, 3);?>
                            >
                                B1
                            </option>
                            <option
                                value="4"
                                <?=selectedHelper($level, 4);?>
                            >
                                B2
                            </option>
                        </optgroup>		
                        <optgroup label="Avancé">
                            <option
                                value="5"
                                <?=selectedHelper($level, 5);?>
                            >
                                C1
                            </option>
                            <option
                                value="6"
                                disabled
                            >
                                C2 (indisponible)
                            </option>
                        </optgroup>
                    </select>
                </div>
                <div>
                    <label for="date">Date du cours&nbsp;:&nbsp</label>
                    <input
                        type="date"
                        id="date"
                        name="date"
                        value="<?=getOrDefault("date");?>"
                        required>
                </div>
                <div>
                    <label>Créneau(x) horaire(s):</label>
                    <div>
                        <div>
                            <input
                                type="checkbox"
                                id="morning"
                                name="morning"
                                value="morning"
                                <?=checkedHelper("morning");?>
                            >
                            <label for="morning">
                                matin
                            </label>
                        </div>
                        <div>
                            <input
                                type="checkbox"
                                id="afternoon"
                                name="afternoon"
                                value="afternoon"
                                <?=checkedHelper("afternoon");?>
                            >
                            <label for="afternoon">
                                après-midi
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Autre</legend>
            <div>
                <div>
                    <label for="password">
                        Mot de passe&nbsp;:&nbsp;
                    </label>
                    <input
                        id="password"
                        type="password"
                        name="password"
                        required>
                </div>
                <div>
                    <label for="misc">
                        Information complémentaire optionelle&nbsp;:
                    </label>
                    <textarea
                        id="misc"
                        name="message"
                        rows="3"
                        cols="42">
                    </textarea>
                </div>
            </div>
        </fieldset>

        <input type="submit" value="Valider"> 
        <input type="reset" value="Réinitialiser">
    </form>
</section>
<section>
    <h2>Récupérer des informations à partir d'un code</h2>
    <p>Si vous voulez préremplir le formulaire plus haut pour vérifier les
        informations à partir d'un code donné ou le resoumettre en altérant
        votre inscription, remplissez ce formulaire avec votre code et mot de
        passe (note: le dernier champs optionnel est ignoré). Une seule
        inscription est valide à la fois et une resoumission remplacera
        l'inscription précédente portant le même couple (nom, prénom). </p>
    <form method="post" action="souscription.php">
        <fieldset>
            <legend>Récupérateur</legend>
            <div>
                <div>
                    <label for="code">Code&nbsp;:&nbsp</label>
                    <input
                        type="text"
                        id="code"
                        name="code"
                        required>
                </div>
                <div>
                    <label for="recoverypassword">
                        Mot de passe&nbsp;:&nbsp;
                    </label>
                    <input
                        id="recoverypassword"
                        type="password"
                        name="recoverypassword"
                        required>
                </div>
            </div>
        </fieldset>
        <input type="submit" value="Valider"> 
        <input type="reset" value="Réinitialiser">
    </form>
</section>

<?php include('include/articleFooter.php'); ?>
