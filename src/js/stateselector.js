// Optional javascript file used to hide one of the two methods on method.php
// and make the .state-selector's div clickable.
window.onload = function() {
    const showEpiMethod = document.getElementById('show-epi-method');
    if (showEpiMethod) {
        showEpiMethod.addEventListener('change', function() {
            const sections = document.getElementsByTagName('section');
            if (!showEpiMethod.checked) {
                sections[3].style.display = 'none';
                sections[2].style.display = 'block';
            } else {
                sections[2].style.display = 'none';
                sections[3].style.display = 'block';
            }
        });
        showEpiMethod.dispatchEvent(new Event('change'));
    }
    const ss = document.getElementsByClassName('state-selector');
    for (const s of ss) {
        const label = s.getElementsByTagName('label')[0];
        if (label) {
            const checkbox = document.getElementById(label.getAttribute('for'));
            if (checkbox) {
                const checkboxDiv = label.previousElementSibling;
                if (checkboxDiv) {
                    checkboxDiv.addEventListener('click', function() {
                        checkbox.checked = !checkbox.checked;
                        if (checkbox.isEqualNode(showEpiMethod)) {
                            checkbox.dispatchEvent(new Event('change'));
                        }
                    });
                }
            }
        }
    }
}
