<?php 
$title = 'La méthode';
include('include/articleHeader.php');
?>

<section>
    <h1>Présentation de la méthode Gen-eki</h1>
    <p> <span class="gen-eki">Gen-eki</span>(<span lang="ja">現役</span>) en
        japonais peut se traduire par "<em>service actif</em>" et décrit bien
        la caractéristique principale de la méthode&nbsp;: impliquer
        activement l'apprenant dans ses études par un choix préliminaire basé
        sur ses préférences et ensuite le motiver à consommer du contenu natif
        correspondant à ses goûts. Explorons en détails ses caractéristiques.
    </p>
    <h2>Une méthode personnalisée</h2>
    <p> Gen-eki propose deux parcours types correspondants à deux tendances
        principales qui se dégageaient lors de séances avec nos
        étudiants&nbsp;: </p>
    <ol>
        <li>
            Le parcours emmagasinement puis immersion
        </li>
        <li>
            Le parcours immersion en premier lieu
        </li>
    </ol>
    <p> Le premier <strong>parcours emmagasinement</strong> puis immersion
        correspond à la manière classique d'apprendre en se focalisant en
        premier sur le vocabulaire et la grammaire avant de se confronter
        directement à la langue en consommant du contenu natif. Dans le cas de
        l'apprentissage du japonais, une étape supplémentaire y est ajoutée
        avec l'étude des kanji en utilisant un système compréhensif basé sur
        les radicaux de caractères. </p>
    <p> Le second <strong>parcours immersion</strong> en premier lieu, comme
        son nom l'indique, se concentre sur l'apprentissage en contexte à
        l'aide d'étude de documents textuels et sonores complémentés
        d'explications opportunes. Ce parcours est à privilégier pour ceux qui
        se désintéressent de l'apprentissage formel de la grammaire et des
        listes arbitraires de vocabulaire. </p>
    <p> Les deux parcours débouchent sur une longue phase d'immersion
        dans du contenu natif d'un niveau adapté qui valide l'
        <strong>hypothèse d'entrée</strong> telle que décrite par
        Stephen Krashen.</p>
    <blockquote>
        <p> Les apprenants progressent dans leur connaissance de la langue
            lorsqu'ils comprennent une entrée linguistique légèrement plus
            avancée que leur niveau actuel. Ce niveau d'entrée "<var>i</var>+1",
            où "<var>i</var>" est l'interlangue de l'apprenant et
            "+1" le stade suivant de la langue.</p>-
        <cite>Stephen Krashen</cite>
    </blockquote>
    
    <p> Choisissez votre parcours pour plus de détails sur sa chronologie et
        ses éléments constitutifs. </p>
</section>
<section>
    <div class="state-selector">
        <input
            type="checkbox"
            class="toggle-diacritics"
            id="show-epi-method"
        >
        <div>
            <!-- Avoid empty div for HTML5 validator -->
            <div>&nbsp;</div>
            <label for="show-epi-method">
                Basculer vers l'autre méthode
            </label>
        </div>
        <h2>Méthode emmagasinement puis immersion</h2>
        <ol>
            <li>
                <a href="#kanji-epi">Apprendre les kanjis</a>
            </li>
            <li>
                <a href="#kana-epi">Apprendre les kanas</a>
            </li>
            <li>
                <a href="#bases-epi">Apprendre les bases</a>
            </li>
            <li>
                <a href="#immersion-epi">Immersion native</a>
            </li>
        </ol>
        <h2>Méthode immersion en premier lieu</h2>
        <ol>
            <li>
                <a href="#kana-ipl">Apprendre les kanas</a>
            </li>
            <li>
                <a href="#bases-ipl">Apprendre les bases</a>
            </li>
            <li>
                <a href="#immersion-ipl">Immersion native</a>
            </li>
        </ol>
    </div>
</section>
<section>
    <h3 id="kanji-epi">
        <abbr title="Emmagasinement Puis Immersion">EPI</abbr>
        - Apprentissage des kanjis
    </h3>
    <p> Un grand nombre de méthodes repousse à plus tard l'apprentissage des
        kanjis, préférant se concentrer sur la grammaire ou le vocabulaire de
        base. Dans le parcours emmagasinement nous faisont le pari de prendre
        le contre-pied et de construire une base solide grâce à l'étude isolée
        des kanjis. D'après notre expérience, les étudiants maîtrisant les
        kanjis dès le début de leur apprentissage rencontrent très peu de
        difficultés à comprendre et intégrer les concepts introduits dans les
        prochaines étapes du parcours. </p>
    <p> Choisissez et étudiez l'une des trois ressources proposées avant de
        continuer à la prochaine étape&nbsp;: </p>
    <ul>
        <li><cite>Les kanjis dans la tête</cite> par Yves Maniette</li>
        <li>
            <cite>The Kodansha kanji learner's course</cite>
            par Andrew Scott Conning
        </li>
        <li>
            L'application <a href="https://wanikani.com">Wanikani</a>
        </li>
    </ul>

    <h3 id="kana-epi">
        <abbr title="Emmagasinement Puis Immersion">EPI</abbr>
        - Apprentissage des kanas
    </h3>
    <p> Les hiraganas et katakanas forment la base de l'écriture japonaise et
        leur maîtrise est fondamentale afin de comprendre la grammaire même la
        plus basique. Certains manuels proposaient une romanisation de
        l'écriture mais celle-ci ralentissait les apprenants sur le long terme
        et inculquait de mauvaises habitudes. Beaucoup d'instructeurs
        proscrivent aujourd'hui cette pratique. </p>
    <p>Les deux premiers chapitres du premier livre introductif présenté dans
        l'étape suivante traitent des kanas. Nul besoin de ressources
        supplémentaires.</p>

    <h3 id="bases-epi">
        <abbr title="Emmagasinement Puis Immersion">EPI</abbr>
        - Apprentissage du japonais de base
    </h3>
    <p>Pour apprendre une langue, deux constituants principaux sont
        nécessaires&nbsp;: une compréhension de la grammaire ainsi qu'une
        connaissance d'un grand nombre de mots de vocabulaire. Ces deux
        éléments sont souvent fusionnés dans ce que l'on appelle des manuels
        avec méthode intégrée. On présente ici trois manuels à étudier, un pour
        chacun des niveaux novice, débutant et intermédiaire qui vous
        permettront de passer à l'étape suivante une fois maîtrisés.</p>
    <ol>
        <li>
            <cite>
                Genki 1&nbsp;: An Integrated Course in Elementary Japanese
            </cite>
            par Eri Banno 
        </li>
        <li>
            <cite>
                Genki 2&nbsp;: An Integrated Course in Elementary Japanese
            </cite>
            par Eri Banno 
        </li>
        <li>
            <cite>Tobira&nbsp;: Gateway to Advanced Japanese</cite>
            par Mayumi Oka
        </li>
    </ol>

    <h3 id="immersion-epi">
        <abbr title="Emmagasinement Puis Immersion">EPI</abbr>
        - Phase d'immersion dans du contenu natif
    </h3>
    <p>Si vous êtes arrivé jusqu'ici, l'étude formelle de la grammaire et du
        vocabulaire du langage n'est plus nécessaire. La majeure partie du
        temps doit maintenant être consacrée à la consommation de contenu natif
        varié. Cette phase est la plus longue du parcours puisqu'elle ne se
        termine jamais vraiment; après tout, personne ne connaît
        totalement une langue. </p>
    <p>Les premiers média natifs sembleront probablement trop difficiles pour
        vous mais persévérez et vous parlerez japonais à un niveau
        conversationnel après quelques dizaines de romans ou saisons de séries
        télévisées. Continuez sur cette lancée et vous ferez peut-être partie
        des rares apprenants parlant couramment cette langue complexe mais si
        belle.</p>

</section>
<section>
    <h3 id="kana-ipl">
        <abbr title="Immersion en Premier Lieu">IPL</abbr>
        - Apprentissage des kanas
    </h3>
    <p> Les hiraganas et katakanas forment la base de l'écriture japonaise et
        leur maitrise est fondamentale afin de comprendre la grammaire même la
        plus basique. Certains manuels proposaient une romanisation de
        l'écriture mais celle-ci ralentissait les apprenants sur le long terme
        et inculquait de mauvaises habitudes. Beaucoup d'instructeurs
        proscrivent aujourd'hui cette pratique. </p>
    <p>L'apprentissage des kanas représente la seule étape du parcours non
        immersive puisqu'il faut savoir déchiffrer les charactères avant de
        pouvoir consommer du contenu japonais. Choisissez la référence de votre
        choix pour apprendre ces syllabaires.</p>

    <h3 id="bases-ipl">
        <abbr title="Immersion en Premier Lieu">IPL</abbr>
        - Apprentissage du japonais de base
    </h3>
    <p>Si il est généralement conseillé d'apprendre la grammaire et le
        vocabulaire de base en se référant à un manuel intégré, il est tout à
        fait possible de faire sans à partir du moment où les sources de
        grammaire et de vocabulaires sont d'un niveau adapté pour l'apprenant.
        Cet état de fait rend possible l'apprentissage en immersion dès le
        commencement grâce au principe de l'hypothèse d'entrée i+1.</p>
    <p>Choisissez un contenu natif de votre niveau et étudiez-le,
        phrase par phrase, en vous aidant d'une référence de grammaire
        de votre choix ainsi que d'un dictionnaire japonais comme
        <a href="https://jisho.org">jisho.org</a>. Il est conseillé de
        commencer par des supports textuels avant d'étudier des
        contenus audio ou vidéo. Après une douzaine de supports natifs,
        procéder à l'étape suivante.</p>
    <h3 id="immersion-ipl">
        <abbr title="Immersion en Premier Lieu">IPL</abbr>
        - Phase d'immersion dans du contenu natif
    </h3>
    <p>Si vous êtes arrivé jusqu'ici, l'étude formelle de la grammaire et du
        vocabulaire du langage n'est plus nécessaire. La majeure partie du
        temps doit être consacrée à la consommation de contenu natif et varié.
        Cette phase est la plus longue du parcours puisqu'elle ne se termine
        jamais vraiment; après tout, personne ne connaît totalement
        une langue. </p>
    <p>Vous avez propablement consommé de nombreux média en suivant le parcours
        d'Immersion en Premier Lieu. Il s'agit ici de varier le type et
        d'augmenter la difficulté du contenu pour se familiariser avec tous les
        aspects de la langue. Vous ne devriez avoir que peu de problèmes à
        parler couramment japonais; il s'agit principalement d'une question de
        temps et une cinquantaine de productions culturelles plus tard, cette
        destination vous attend certainement.</p>
</section>

<?php include('include/articleFooter.php'); ?>
