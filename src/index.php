<?php
$title = 'Accueil';
include('include/header.php');
?>

<section>
    <div>
        <h1>Offre de formation et contexte</h1>
        <div>
            <div>
                <p> Avec l'essort de l'importation et l'influence grandissante
                    des œuvres japonaises de toutes sortes ces 10 dernières
                    années, de nombreuses personnes à travers le monde tentent
                    de s'ouvrir à la culture de l'archipel nippon à travers
                    l'apprentissage de sa langue. Si le nombre de nouveaux
                    apprenants est en constante croissance, les statistiques de
                    tests standardisés sont sans-appel&nbsp;: le nombre de
                    personne pouvant parler courramment japonais reste
                    extrêmement faible et stagnant en proportion du nombre
                    d'inscris. </p>
                <p> Vous, aussi, avez peut-être débuté votre apprentissage de
                    façon chaotique en choississant aléatoirement un support de
                    cours et en accumullant les questions non-élucidées, les
                    frustrations et les manques de direction criants. La
                    méthode présentée sur ce site est claire, modulable et
                    déterministe. Parcourez les prochaines sections thématiques
                    pour plus d'informations. </p>
            </div>
            <aside lang="ja">理</aside>
        </div>
    </div>
</section>
<section>
    <div>
        <h2>Les difficultés spécifiques de la langue japonaise</h2>
        <div>
            <aside lang="ja">語</aside>
            <div>
                <p> Contrairement à la plupart des langues indo-européennes, le
                    japonais possède un grand nombre de spécificités
                    grammaticales et syntaxiques qui ne manqueront pas de
                    désespérer même le débutant le plus coriace;
                    cependant c'est bien la complexité du système d'écriture
                    qui de prime abord plonge la plupart des étudiants dans un
                    profond désarroi. </p>
                <p> Composée de trois principaux systèmes graphiques auquels on
                    peut ajouter un système de numérotation hybride et
                    l'alphabet latin que nous connaissons, l'écriture japonaise
                    est probablement un des plus sinon le plus compliqué des
                    systèmes d'écriture au monde. </p>
            </div>
        </div>
        <a href="langage.php" class="button">
            Comprendre les systèmes graphiques
        </a>
    </div>
</section>
<section>
    <div>
        <h2>Notre méthode</h2>
        <div>
            <div>
                <p>Fort de 15 ans d'expérience dans le domaine de
                    l'apprentissage linguistique, notre équipe de professionels
                    s'est basée sur les problèmes récurrents rencontrés par les
                    apprenants ainsi que leurs commentaires pour fonder les
                    bases de notre outil pédagogique. Les retours
                    de nos étudiants lorsque soumis à notre méthode
                    nous on permit de dégager plusieurs profils types.</p>
                <p>La conséquence ? Notre cours a profondemment évolué et met à
                    présent à disposition différents parcours d'apprentissage
                    en fonction des besoins et affinités des étudiants, faisant
                    de la méthode <span class="gen-eki">Gen-eki</span>
                    (<span lang="ja">現役</span>) le moyen le plus
                    personnalisable qui soit pour apprendre la langue
                    japonaise.</p>
            </div>
            <aside lang="ja">法</aside>
        </div>
        <a href="methode.php" class="button">
            Apprendre la méthode
        </a>
    </div>
</section>
<section>
    <div>
        <h2>Souscription au service</h2>
        <div>
            <aside lang="ja">録</aside>
            <div>
                <p>Si la méthode présentée est implémentable gratuitement par
                    tout apprenant motivé, il faut reconnaître que
                    l'apprentissage d'une langue étrangère représente une tâche
                    collosale, surtout dans le cas d'une langue comme le
                    japonais partageant aussi peu de similarités avec le
                    français.</p>
                <p>Dans le but d'aider l'étudiant à progresser durant les
                    quelques milliers d'heures nécessaires à l'apprentissage de
                    la langue et afin de rendre les sessions d'étude plus
                    interactives, nos services proposent depuis peu des cours
                    en ligne dont la réservation est possible sur ce
                    site-même.</p>
            </div>
        </div>
        <a href="souscription.php" class="button">
            Souscrire au cours
        </a>
    </div>

</section>

<?php include('include/footer.php'); ?>
