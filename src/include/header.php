<?php

/* Cipher/decipher message using Vigenere cipher. */
/* Characters not matching alphabet chars or digits are kept as-is. */
function vigenere($text, $pass, $cipher=true) {
    $res = "";
    $j = 0;
    $pass = strtolower($pass);
    $passLength = strlen($pass);
    for ($i = 0; $i < strlen($text); $i++) {
        if (ctype_alpha($text[$i]) || ctype_digit($text[$i])) {
            $offset = ord($pass[$j]) - ord("a");
            $base = ord("a");
            if (ctype_upper($text[$i])) {
                $base = ord("A");
            }
            if (ctype_alpha($text[$i])) {
                if ($cipher) {
                    $res .= chr((($offset + ord($text[$i]) - $base) % 26)
                        + $base);
                } else {
                    $offset = ord($text[$i]) - $base - $offset;
                    if ($offset < 0) {
                        $offset += 26;
                    }
                    $res .= chr($offset + $base);
                }
            } elseif (ctype_digit($text[$i])) {
                $offset = ((int)$text[$i]) + ord($pass[$j]);
                if ($cipher) {
                    $res .= $offset % 10;
                } else {
                    $offset = ((int)$text[$i]) - ord($pass[$j]);
                    while ($offset < 0) {
                        $offset += 10;
                    }
                    $res .= $offset;
                }
            }
            $j++;
            if ($j >= $passLength) {
                $j = 0;
            }
        } else {
            $res .= $text[$i];
        }
    }
    return $res;
}

/* Trim pesky non-printable characters to avoid bakemojis or non-idempotent */
/* vigenere function calls and then escape html/js to avoid code injection. */
function sanitize($input) {
    return htmlspecialchars(trim($input));
}

/* Decipher code with recoverypassword and create a get params uri from it. */
function getParamsFromCode() {
    $code = sanitize($_POST["code"]);
    $pass = sanitize($_POST["recoverypassword"]);
    $v = explode("_", trim(vigenere($code, $pass, false)));
    $k = ["surname", "forename", "phone", "language", "level", "date"];
    /* Even if injection after vigenere is unlikely, let's sanitize still. */
    $v = array_map("sanitize", $v);
    /* Special handling of checkboxes. */
    for ($i = 6; $i < count($v); $i++) {
        array_push($k, sanitize($v[$i]));
    }
    return "?" . http_build_query(array_combine($k, $v));
}

/* Redirection must occur before any html output so we put this here. */
if (isset($_POST["recoverypassword"]) && isset($_POST["code"])) {
    $cleanURI = trim(strtok($_SERVER["REQUEST_URI"], "?"));
    $url = "http://$_SERVER[HTTP_HOST]" . $cleanURI . getParamsFromCode();
    header('Location: ' . $url);
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
    >
    <meta charset="utf-8">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
    <script src="js/stateselector.js"></script>
    <title>Gen-eki | <?=$title;?></title>
</head>
<body>
<header>
    <div>
        <div lang="ja">
            <div><span class="hexagontext">現</span></div>
            <div><span class="hexagontext">役</span></div>
        </div>
        <p>
            La méthode Gen-eki
        </p>
        <p>
            Une façon moderne d'apprendre le japonais.
        </p>
    </div>
</header>
<nav>
    <ul>
        <li><a href="index.php">accueil</a></li>
        <li><a href="langage.php">langage</a></li>
        <li><a href="methode.php">méthode</a></li>
        <li><a href="souscription.php">souscription</a></li>
    </ul>
</nav>
<main>
