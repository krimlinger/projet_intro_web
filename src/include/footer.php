</main>
<footer>
    <div>
        <p>&copy; Créé par Rimlinger Ken</p>
        <p>Image de la bannière par
            <a href="https://www.freepik.com/">Freepik</a>.
        </p>
    </div>
    <address>
        <p>Pour plus de détails, contacter
            <a href="mailto:ken.rimlinger1@etu.univ-lorraine.fr">le webmestre</a>.
        </p>
    </address>
</footer>
</body>
</html>
