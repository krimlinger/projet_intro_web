<?php 
$title = 'Le langage';
include('include/articleHeader.php');
?>

<section>
    <h1>Un système d'écriture complexe</h1>
    <p>Savez-vous ce qu'est un <em>cognat</em> ? Faisons une expérience de
        pensée. Prenez un locuteur natif d'une langue latine et faites-le lire
        un article de journal dans une autre langue latine qu'il ne parle pas.
        Il est probable que ce locuteur comprenne les grandes lignes de
        l'article sans avoir étudié la langue au préalable. Les raisons de cet
        état de fait&nbsp;: une grammaire commune ainsi que la présence de cognats,
        ou mots apparentés. </p>
    <p>Une grammaire semblable ainsi que des mots ayant une étymologie commune
        simplifie grandement l'apprentissage d'une langue étrangère. Ainsi, les
        coréens possèdent un avantage considérable de par leur grammaire
        semblable au japonais alors que les chinois maîtrisent souvent
        rapidement un des composants du système d'écriture japonais&nbsp;: les
        kanjis. L'étudiant occidental moyen cependant ne possède pas de tels
        avantages et devra redoubler d'effort pour se familiariser avec une
        combinaison de <strong>trois principaux systèmes d'écriture</strong>
        que nous allons maintenant présenter. </p>
    <h2>Les kanas</h2>
    <p>La langue japonaise ne se compose pas de lettres de l'alphabet latin
        mais utilise grandement deux syllabaires possédant des caractères
        associés à des sons. Ces deux syllabaires sont les
        <strong>hiraganas</strong> et les <strong>katakanas</strong> et
        possèdent les mêmes 46 sons utilisés dans le japonais moderne, chacun
        associé de façon symétrique à un caractère différent provenant d'un des
        syllabaires. </p>
    <h3>Les hiraganas</h3>
    <p>Les <strong>hiraganas</strong> composent le premier syllabaire. De
        formes rondes et douces, ils sont utilisés pour les mots natifs du
        japonais ainsi que pour un grand nombre de fonctions grammaticales
        comme la conjugaison ou la spécification de cas par usage de
        particules. </p>
    <p>Historiquement, les hiraganas étaient appris dans l'ordre
        correspondant à un poème classique appelé l'<cite>Iroha Uta
        (<span lang="ja">いろは歌</span>)</cite> ou <cite>Chant de 
        l'Iroha</cite>, qui possède la particularité d'être composé de 
        chacun des hiraganas existants une seule fois (à l'exception de
        <span lang="ja">ん</span>). Ce poème est encore utilisé aujourd'hui
        pour se référer aux noms de notes de musique ou ordonner les
        articles des textes législatifs. Ce site utilise aussi cette
        numérotation pour les listes à puces. Une variante moderne de
        l'Iroha est présentée par la suite.</p>
    <!-- <br> in a poem represents a "usefull newline " -->
    <p class="poem" lang="ja">
        <span>い</span>ろはにおえど<br>
        <span>ち</span>りぬるを<br>
        <span>わ</span>がよたれぞ<br>
        <span>つ</span>ねならん<br>
        <span>う</span>いのやま<br>
        <span>き</span>ょうこえて<br>
        <span>あ</span>さきゆめみじ<br>
        <span>よ</span>いもせず
    </p>
    <p>
        Si l'Iroha était utilisé par le passé comme système de numérotation 
        avec une relation d'ordre relativement arbitraire, de nos jours
        un autre système, plus logique, nommé <strong>gojuuon
        (<span lang="ja">五十音</span>)</strong> lui a succédé.
        Un tableau référençant tous les hiraganas dans cette numérotation
        est présenté plus bas, l'ordre canonique étant établi par une
        lecture de gauche à droite ("sons voyelles" <em>a</em>&rarr;
        <em>i</em>&rarr;<em>u</em>&rarr;<em>e</em> &rarr;<em>o</em>)
        puis de bas en haut (ajout d'un modificateur "son consomne" devant
        le "son voyelle").
    </p>
    <p>
        On remarquera que le japonais possède aussi deux caractères 
        <strong>diacritiques</strong> appelés <em>Dakuten et Handakuten
            (<span lang="ja">半|濁点</span>)</em> permettant de voiser le son
        consomne. Ces hiraganas voisés ne font pas partie de l'ordre
        canonique gojuuon.
    </p>
    <div class="state-selector">
        <input
            type="checkbox"
            class="toggle-diacritics"
            id="show-hiragana-diacritics"
        >
        <div>
            <!-- Avoid empty div for HTML5 validator -->
            <div>&nbsp;</div>
            <label for="show-hiragana-diacritics">
                Afficher/Cacher les diacritiques
            </label>
        </div>
        <table>
            <caption>Syllabaire hiragana</caption>
            <thead>
                <tr>
                    <th> </th>
                    <th>a</th>
                    <th>i</th>
                    <th>u</th>
                    <th>e</th>
                    <th>o</th>
                </tr>
            </thead>
            <tbody lang="ja">
                <tr>
                    <th lang="fr">∅</th>
                    <td>あ</td>
                    <td>い</td>
                    <td>う</td>
                    <td>え</td>
                    <td>お</td>
                </tr>
                <tr>
                    <th lang="fr">k</th>
                    <td>か</td>
                    <td>き</td>
                    <td>く</td>
                    <td>け</td>
                    <td>こ</td>
                </tr>
                <tr>
                    <th lang="fr">g</th>
                    <td>が</td>
                    <td>ぎ</td>
                    <td>ぐ</td>
                    <td>げ</td>
                    <td>ご</td>
                </tr>
                <tr>
                    <th lang="fr">s</th>
                    <td>さ</td>
                    <td>し</td>
                    <td>す</td>
                    <td>せ</td>
                    <td>そ</td>
                </tr>
                <tr>
                    <th lang="fr">z</th>
                    <td>ざ</td>
                    <td>じ</td>
                    <td>ず</td>
                    <td>ぜ</td>
                    <td>ぞ</td>
                </tr>
                <tr>
                    <th lang="fr">t</th>
                    <td>た</td>
                    <td>ち</td>
                    <td>つ</td>
                    <td>て</td>
                    <td>と</td>
                </tr>
                <tr>
                    <th lang="fr">d</th>
                    <td>だ</td>
                    <td>ぢ</td>
                    <td>づ</td>
                    <td>で</td>
                    <td>ど</td>
                </tr>
                <tr>
                    <th lang="fr">n</th>
                    <td>な</td>
                    <td>に</td>
                    <td>ぬ</td>
                    <td>ね</td>
                    <td>の</td>
                </tr>
                <tr>
                    <th lang="fr">h</th>
                    <td>は</td>
                    <td>ひ</td>
                    <td>ふ</td>
                    <td>へ</td>
                    <td>ほ</td>
                </tr>
                <tr>
                    <th lang="fr">b</th>
                    <td>ば</td>
                    <td>び</td>
                    <td>ぶ</td>
                    <td>べ</td>
                    <td>ぼ</td>
                </tr>
                <tr>
                    <th lang="fr">p</th>
                    <td>ぱ</td>
                    <td>ぴ</td>
                    <td>ぷ</td>
                    <td>ぺ</td>
                    <td>ぽ</td>
                </tr>
                <tr>
                    <th lang="fr">m</th>
                    <td>ま</td>
                    <td>み</td>
                    <td>む</td>
                    <td>め</td>
                    <td>も</td>
                </tr>
                <tr>
                    <th lang="fr">y</th>
                    <td>や</td>
                    <td></td>
                    <td>ゆ</td>
                    <td></td>
                    <td>よ</td>
                </tr>
                <tr>
                    <th lang="fr">r</th>
                    <td>ら</td>
                    <td>り</td>
                    <td>る</td>
                    <td>れ</td>
                    <td>ろ</td>
                </tr>
                <tr>
                    <th lang="fr">w</th>
                    <td>わ</td>
                    <td colspan="3"></td>
                    <td>を</td>
                </tr>
                <tr>
                    <th></th>
                    <td colspan="5">ん</td>
                </tr>
            </tbody>
        </table>
    </div>
    <h3>Les katakanas</h3>
    <p>Crées bien après les hiraganas, les <strong>katakanas</strong> sont des
        caractères à la forme plus droite et austère. Ils sont principalement
        utilisé pour signifier que le mot a une origine étrangère ou qu'il
        représente une onomatopée.</p>
    <p> Les katakanas formaient un syllabaire symétriques au syllabaire
        hiragana mais avec le temps a évolué. Certaines combinaisons de
        katakanas qui lui sont propres ont ainsi fait leur apparition pour
        former des sons inéxistants originalement dans la langue japonaise et
        permettant de mieux retranscrire certains mots d'emprunt étranger. Ces
        caractéristiques propres sont probablement mieux apprises sur le tas,
        avec les mots de vocabulaires les utilisant et ne figurent pas dans le
        tableau récapitulatif suivant. </p>
    <div class="state-selector">
        <input
            type="checkbox"
            class="toggle-diacritics"
            id="show-katakana-diacritics"
        >
        <div>
            <!-- Avoid empty div for HTML5 validator -->
            <div>&nbsp;</div>
            <label for="show-katakana-diacritics">
                Afficher/Cacher les diacritiques
            </label>
        </div>
        <table>
            <caption>Syllabaire katakana</caption>
            <thead>
                <tr>
                    <th> </th>
                    <th>a</th>
                    <th>i</th>
                    <th>u</th>
                    <th>e</th>
                    <th>o</th>
                </tr>
            </thead>
            <tbody lang="ja">
                <tr>
                    <th lang="fr">∅</th>
                    <td>ア</td>
                    <td>イ</td>
                    <td>ウ</td>
                    <td>エ</td>
                    <td>オ</td>
                </tr>
                <tr>
                    <th lang="fr">k</th>
                    <td>カ</td>
                    <td>キ</td>
                    <td>ク</td>
                    <td>ケ</td>
                    <td>コ</td>
                </tr>
                <tr>
                    <th lang="fr">g</th>
                    <td>ガ</td>
                    <td>ギ</td>
                    <td>グ</td>
                    <td>ゲ</td>
                    <td>ゴ</td>
                </tr>
                <tr>
                    <th lang="fr">s</th>
                    <td>サ</td>
                    <td>シ</td>
                    <td>ス</td>
                    <td>セ</td>
                    <td>ソ</td>
                </tr>
                <tr>
                    <th lang="fr">z</th>
                    <td>ザ</td>
                    <td>ジ</td>
                    <td>ズ</td>
                    <td>ゼ</td>
                    <td>ゾ</td>
                </tr>
                <tr>
                    <th lang="fr">t</th>
                    <td>タ</td>
                    <td>チ</td>
                    <td>ツ</td>
                    <td>テ</td>
                    <td>ト</td>
                </tr>
                <tr>
                    <th lang="fr">d</th>
                    <td>ダ</td>
                    <td>ジ</td>
                    <td>ズ</td>
                    <td>ゼ</td>
                    <td>ゾ</td>
                </tr>
                <tr>
                    <th lang="fr">n</th>
                    <td>ナ</td>
                    <td>ニ</td>
                    <td>ヌ</td>
                    <td>ネ</td>
                    <td>ノ</td>
                </tr>
                <tr>
                    <th lang="fr">h</th>
                    <td>ハ</td>
                    <td>ヒ</td>
                    <td>フ</td>
                    <td>ヘ</td>
                    <td>ホ</td>
                </tr>
                <tr>
                    <th lang="fr">b</th>
                    <td>バ</td>
                    <td>ビ</td>
                    <td>ぶ</td>
                    <td>ベ</td>
                    <td>ボ</td>
                </tr>
                <tr>
                    <th lang="fr">p</th>
                    <td>パ</td>
                    <td>ピ</td>
                    <td>プ</td>
                    <td>ペ</td>
                    <td>ポ</td>
                </tr>
                <tr>
                    <th lang="fr">m</th>
                    <td>マ</td>
                    <td>ミ</td>
                    <td>ム</td>
                    <td>メ</td>
                    <td>モ</td>
                </tr>
                <tr>
                    <th lang="fr">y</th>
                    <td>ヤ</td>
                    <td></td>
                    <td>ユ</td>
                    <td></td>
                    <td>ヨ</td>
                </tr>
                <tr>
                    <th lang="fr">r</th>
                    <td>ラ</td>
                    <td>リ</td>
                    <td>ル</td>
                    <td>レ</td>
                    <td>ロ</td>
                </tr>
                <tr>
                    <th lang="fr">w</th>
                    <td>ワ</td>
                    <td colspan="3"></td>
                    <td>ヲ</td>
                </tr>
                <tr>
                    <th></th>
                    <td colspan="5">ン</td>
                </tr>
            </tbody>
        </table>
    </div>

    <h2>Les kanjis</h2>
    <p> Le dernier composant du système d'écriture japonais, et non des
        moindres, est le <strong>kanji</strong>. Ces caractères d'origine
        chinoise sont souvent de nature plus complexe que les kanas de par la
        grande variété des composants les constituants mais aussi à cause du
        grand nombre de prononciations qu'un même caractère peut avoir selon le
        type de mot auquel il appartient. Pour lire du japonais courrant, on
        estime nécessaire de connaître entre <em>3000 et 4000</em> kanjis.
        L'étude de ces caractères représentera donc une grande partie du temps
        consacré à l'apprentissage de la langue. </p>
    <p> Les vagues successives d'importation de caractères à des périodes où la
        langue chinoise a souvent évoluée, notamment en terme de prononciation,
        sont une des raisons de la grande variété de sons associés à un même
        kanji. Il faut ajouter à ces prononciations sino-japonaises (lecture
        ON/<span lang="ja">音読み</span>) la prononciation purement japonaise
        (lecture KUN/ <span lang="ja">訓読み</span>) qui est souvent associée à
        des mots japonais qui plus tard se sont adaptés et ont obtenu une
        orthographe faisant usage d'un kanji. Apprendre la prononciation des
        kanji représente une des parties majeures de l'apprentissage de la
        langue japonaise; aussi, il est préférable d'apprendre les
        kanjis en contexte, avec le mot de vocabulaire l'utilisant plutôt que
        de mémoriser toutes les prononciations potentielles d'un même
        caractère. </p>
    <map name="ai-kanji">
        <area
            shape="poly"
            coords="50,85,160,5,235,65,195,85,90,95"
            href="https://jisho.org/search/%E7%88%AA%20%23kanji"
            alt="Composant serre d'oiseau du kanji de l'amour platonique"
            target="_blank"
        >
        <area
            shape="poly"
            coords="25,150,17,125,33,90,57,104,150,90,297,89,297,115,252,128,252,100,145,102,56,117"
            href="https://jisho.org/search/%E5%86%96%20%23kanji"
            alt="Composant toît/couronne du kanji de l'amour platonique"
            target="_blank"
        >
        <area
            shape="rect"
            coords="57,118,250,165"
            href="https://jisho.org/search/%E5%BF%83%20%23kanji"
            alt="Composant cœur du kanji de l'amour platonique"
            target="_blank"
        >
        <area
            shape="poly"
            coords="101,166,210,166,291,295,9,290,68,216"
            href="https://jisho.org/search/%E5%A4%82%20%23kanji"
            alt="Composant jambe/retard du kanji de l'amour platonique"
            target="_blank"
        >
    </map>
    <div>
        <figure>
            <img
                alt="Image du Kanji japonais de l'amour platonique"
                src="img/japanese-love-kanji.png"
                usemap="#ai-kanji"
            >
            <figcaption>
                Cliquez sur un composant du kanji pour en savoir plus.
            </figcaption>
        </figure>
        <div>
            <p> Une autre particularité des kanjis est l'étendue des composants
                les constituants. Un érudit de la langue décomposera un kanji
                en ses différents éléments pour en extraire un sens
                <em>sémantique</em> ou <em>phonologique</em> alors qu'un
                débutant ne pourra rien interpréter de ce qu'il considère comme
                une volée de coups de pinceaux proche d'un gribouillage. </p>
            <p> Mettre l'accent sur l'apprentissage des
                <strong>radicaux</strong>, ces composants récurrents
                intervenant dans l'écriture des kanjis est une excellente idée
                pour permettre de cimenter les kanjis en mémoire. Par ailleurs,
                la plupart des méthodes  d'apprentissage se concentrent sur une
                approche donnant la priorité à l'étude sémantique de ces
                composants car il est plus facile de retenir et comprendre les
                caractères en tant que somme de petits éléments clairement
                définis. </p>
        </div>
    </div>
</section>

<?php include('include/articleFooter.php'); ?>
