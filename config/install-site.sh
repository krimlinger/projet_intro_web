#!/usr/bin/env sh

set -e

# Edit theses variables according to your environment.
nginx_conf_dir=/etc/nginx
nginx_www_dir=/var/www
project_name=projetweb

# Start of script, do not edit anything further down if unsure.
project_conf="sites-available/$project_name"

cp -v "nginx/$project_conf" "$nginx_conf_dir/$project_conf"
ln -vsf "$nginx_conf_dir/$project_conf" "$nginx_conf_dir/sites-enabled/$project_name"

if [ ! -f "/etc/nginx/snippets/fastcgi-php.conf" ]; then
    mkdir -vp "$nginx_conf_dir/snippets"
    cp -v "nginx/snippets/fastcgi-php.conf" "$nginx_conf_dir/snippets/fastcgi-php.conf"
fi

ln -vfs "$(dirname "$PWD")/src" "$nginx_www_dir/$project_name"

systemctl restart nginx
