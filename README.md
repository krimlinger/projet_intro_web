# Projet d'introduction au web

## Installation

Ce projet utilise php et doit donc être servi par un server web muni d'un
module FastCGi, CGI ou équivalent. Les fichiers à servir sont localisés
dans le repertoire `src`.

Pour une installation simple sous système basé sur Debian avec nginx
et php-fpm, un script de configuration est fourni. Ce script utilise
la configuration duale `sites-enabled`/`sites-available` et monte le projet
à l'uri [/projetweb](http://127.0.0.1/projetweb/).

    $ sudo apt install nginx php-fpm
    $ cd config
    $ sudo ./install-site.sh
    $ sudo systemctl restart nginx

Ne pas oublier de fournir les droits d´accès à l´utilisateur `www-data` utilisé
par nginx au repertoire du projet et de supprimer la configuration de base
qui route tout sur `/index.html`.

## Licence

Sauf exception, les fichiers sont tous distribués sous licence AGPLv3.
La police de caractère Lobster est originellement sous licence Apache.


### Exceptions notables

Les polices de caractère Lobster et AC Kaisho sont distribuées sous licence SIL
qui a certaines incompatibilités potentielles avec l'AGPL. Étant des éléments
configurables sans fort couplage avec le reste du code, leur inclusion reste
permise dans `src/fonts` et leur usage est modulable par l'édition du fichier
`src/css/style.css`.

L'image en bannière fait aussi office d'exception en tant que "donnée"
de la même manière que les polices de caractère précédentes et est fournie par
[Freepik](https://Freepik.com) sous une licence "free" avec attribution.
